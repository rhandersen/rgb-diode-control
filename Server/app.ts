/// <reference path="../typings/node/node.d.ts"/>
class App {
    arduinoManager : ArduinoManager;
    socketServer : SocketServer;
    
	constructor(){
        var self = this;
		self.arduinoManager = new ArduinoManager("/dev/cu.usbmodem1411", function(){
            self.socketServer = new SocketServer(1337, self.colorObjectReceived(self));
        });
	}
    
    public colorObjectReceived(context : any){
        // Use currying to set the right context
        return function(message : any){
            var self = context;
            self.socketServer.multicastToClients(message.utf8Data);
            var color = JSON.parse(message.utf8Data);
            self.arduinoManager.setAnalogValue(9, color.red);
            self.arduinoManager.setAnalogValue(10, color.green);
            self.arduinoManager.setAnalogValue(11, color.blue);
        }
    }
}

class ArduinoManager {
    values : Array<number>;
    board : any;
    bufferSize : number = 10;
    
    constructor(path : string, readyCallback : Function){
        var self = this;
        this.board = null;
        this.values = []; 
        self.connect(path, readyCallback);
    }
    
    private connect(path : string, readyCallback : Function){
        var self = this;
        console.log("Connecting to Arduino");
        var firmata = require('firmata');
        self.board = new firmata.Board(path, function(){
            console.log("Connected to Arduino");
            readyCallback();
        });  
    }
    
    public getLastValue(){
        if(this.values.length > 0){
            return this.values[this.values.length-1];
        } else {
            return 0;
        }
    }
    
    public getValueBuffer() : Array<number>{
        return this.values;
    }
    
    public subscribeToAnalogValue(pinNumber : number){
        var self = this;
        self.board.pinMode(pinNumber, self.board.MODES.ANALOG);
        self.board.analogRead(pinNumber, function(value : number){
            self.values.push(value);
            if(self.values.length > self.bufferSize){
                self.values.shift();
            }
        });
    }
    
    public setAnalogValue(pinNumber : number, value : number){
        var self = this;
        if(value <= 255 && value >= 0){
            self.board.pinMode(pinNumber, self.board.MODES.PWM);
            self.board.analogWrite(pinNumber, value);
        }
    }
}

class SocketServer {
	httpServer : any;
    socketServer : any;
	clients : Array<any>;
	
	constructor(port : number, messageHandler : Function) {
		var self = this;
		var WebSocketServer = require('websocket').server;
		var http = require('http');
		this.clients = [];
		this.httpServer = http.createServer(function(request, response) {
            // process HTTP request
        });
        this.httpServer.listen(port, function() { 
                
        });
        this.socketServer = new WebSocketServer({
            httpServer: self.httpServer
        });
        
        self.socketServer.on('request', function(request) {
            console.log("Client connected");
            var connection = request.accept(null, request.origin);
            self.clients.push(connection);
            
            // Attach message handler
            connection.on('message', messageHandler);
                
            // Attach close handler
            connection.on('close', function(connection) {
                
            });
        });
		
        // Attach close handler
		self.socketServer.on('close', function(connection){
            
		});
	}
	
	public multicastToClients(message : string){
		var self = this;
		self.clients.forEach(client => {
        	client.send(message);
        });
	}
}

new App();

