### What is this repository for? ###
Proof-of-concept application for controlling the pins of an Arduino from a browser through Firmata and Node.js, in this case for controlling an RGB diode with a color-picker in the browser. 

### How do I get set up? ###
TypeScript files should be compiled, Firmata installed on Arduino and Node.js installed on computer. Inside the "Server" folder is "App" located which should be run with Node.js. The "Client" folder is for the browser -> Open client.html to connect to the server.

### Dependencies ###
* Client-app: Knockout.js, JQuery, Bootstrap
* Server-app: Firmata-module, Node.js
* Arduino: Firmata