/// <reference path="../typings/knockout/knockout.d.ts"/>
/// <reference path="../typings/node/node.d.ts"/>
/// <reference path="../typings/jquery/jquery.d.ts"/>

class ColorConverter {
    private static componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    public static rgbToHex(r, g, b) {
        return "#" + ColorConverter.componentToHex(r) + ColorConverter.componentToHex(g) + ColorConverter.componentToHex(b);
    }
    
    public static hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            red: parseInt(result[1], 16),
            green: parseInt(result[2], 16),
            blue: parseInt(result[3], 16)
        } : null;
    }
}

class AppViewModel {
   canSend: KnockoutObservable<Boolean>;
   color: KnockoutObservable<string>;
   connection: WebSocket;
   
   constructor(){
       var that = this;
       that.canSend = ko.observable(false);
       that.color = ko.observable("#FFFFFF");
       
       this.connection = new WebSocket('ws://localhost:1337');
       
       this.connection.onopen = function (e : Event) {
           that.canSend(true);
       };

        this.connection.onerror = function (e : Event) {
            console.log("An error happened: " + e);
        };

        this.connection.onmessage = function (message : MessageEvent) {
            setTimeout(that.receive(message.data), 0);
        };
        
        this.color.subscribe(function(){
            var colorObject = ColorConverter.hexToRgb(this.color());
            this.send(colorObject.red, colorObject.green, colorObject.blue);
        }, this);
   }
   
   private receive(message : string) : void{
       var self = this;
       var color = JSON.parse(message);
       self.color(ColorConverter.rgbToHex(color.red, color.green, color.blue));
   }
   
   private send(red : number, green: number, blue: number) : void {
       var self = this;
       if(self.canSend()){
           this.connection.send(JSON.stringify({
               'red' : red,
               'green' : green,
               'blue' : blue
           }));
       }
   }
}

ko.applyBindings(new AppViewModel()); 


